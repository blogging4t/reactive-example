package com.ccpatrut.service;

import com.ccpatrut.dto.MessageTO;
import io.smallrye.mutiny.Multi;

public interface MessageService {
    MessageTO save(final MessageTO message);

    Multi<MessageTO> notifySave(final MessageTO message);
}
