package com.ccpatrut.service.impl;

import com.ccpatrut.dto.MessageTO;
import com.ccpatrut.service.MessageService;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.operators.multi.processors.BroadcastProcessor;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MessageServiceImpl implements MessageService {
    private static final BroadcastProcessor<MessageTO> BROADCASTER = BroadcastProcessor.create();

    @Override
    public MessageTO save(final MessageTO message) {
        notifySave(message);
        return message;
    }

    @Override
    public Multi<MessageTO> notifySave(final MessageTO message) {
        if (message != null) {
            BROADCASTER.onNext(message);
        }
        return BROADCASTER;
    }
}
