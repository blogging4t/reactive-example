package com.ccpatrut.api;

import com.ccpatrut.dto.MessageTO;
import com.ccpatrut.service.MessageService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@ServerEndpoint(MessageReactiveResource.PATH)
public class MessageReactiveResource {
    static final String PATH = "/new-message";
    private static final Logger LOGGER = Logger.getLogger(MessageReactiveResource.class);

    private final List<Session> sessions = new CopyOnWriteArrayList<>();

    private final MessageService messageService;

    @Inject
    public MessageReactiveResource(final MessageService messageService) {
        this.messageService = messageService;
    }

    @OnOpen
    public void onOpen(final Session session) {
        sessions.add(session);
    }

    @OnClose
    public void onClose(final Session session) {
        sessions.remove(session);
    }

    @PostConstruct
    public void subscribe() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        messageService.notifySave(null)
                .subscribe()
                .with(message -> sessions.forEach(session -> session.getAsyncRemote().sendText(tryWritingString(message, mapper))));
    }

    @OnError
    public void onError(final Session session, final Throwable throwable) {
        sessions.remove(session);
        LOGGER.error("error: " + throwable.getMessage());
    }

    private String tryWritingString(final MessageTO message, final ObjectMapper mapper) {
        try {
            return mapper.writeValueAsString(message);
        } catch (final JsonProcessingException e) {
            LOGGER.error("Could not serialize object", e);
        }
        return null;
    }
}
