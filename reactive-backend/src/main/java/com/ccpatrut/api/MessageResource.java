package com.ccpatrut.api;

import com.ccpatrut.dto.MessageTO;
import com.ccpatrut.service.MessageService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(MessageResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MessageResource {
    static final String PATH = "/messages";

    private final MessageService messageService;

    @Inject
    public MessageResource(final MessageService messageService) {
        this.messageService = messageService;
    }

    @POST
    public Response save(@Valid @NotNull final MessageTO message) {
        return Response
                .status(Response.Status.CREATED)
                .entity(messageService.save(message))
                .build();
    }
}
