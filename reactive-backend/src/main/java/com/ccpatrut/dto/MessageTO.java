package com.ccpatrut.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.auto.value.AutoValue;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AutoValue
@RegisterForReflection
@JsonPropertyOrder({"username", "message"})
public abstract class MessageTO {

    @NotNull
    @Size(min = 4, max = 255)
    public abstract String getUsername();

    @NotNull
    @Size(min = 4, max = 255)
    public abstract String getMessage();

    @JsonCreator
    public static MessageTO create(@JsonProperty("username") final String username,
                                   @JsonProperty("message") final String message) {
        return builder()
                .username(username)
                .message(message)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_MessageTO.Builder();
    }
    
    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder username(final String newUsername);

        public abstract Builder message(final String newMessage);

        public abstract MessageTO build();
    }
}
